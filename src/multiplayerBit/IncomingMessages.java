package multiplayerBit;

import server.Player;

import java.io.*;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Arrays;

public class IncomingMessages {
    private Thread t;
    public BufferedWriter writerChannel;
    public BufferedReader readerChannel;
    public Player player;

    @Override
    public void run() {
        String line;
        while(true){
            if(t.isInterrupted()){
                return;
            }
            try {
                line = readerChannel.readLine(); // This is similar to listener.accept ; it will wait until there is a message to read.
                if(line != null){
                    ArrayList<String> args = new ArrayList<>(Arrays.asList(line.split(" ")));
                    MDeciphering.gogo(args, player);
                }
            } catch (IOException e) {
                System.out.println("Player " + player.name + " has left the game.");
                Server.world.allPlayers.remove(this.player);
                t.interrupt();
                //e.printStackTrace();

            }

        }
    }
    public IncomingMessages(Socket socket, Player player){
        try{
            readerChannel = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            writerChannel = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));
        }catch(IOException ioe){
            ioe.printStackTrace();
        }
        this.player = player;

    }
    public void start () {
        System.out.println("Message thread created for player " + this.player.name);
        if (t == null) {
            t = new Thread (this, "IncMessagesThread");
            t.start ();
        }
    }
}
