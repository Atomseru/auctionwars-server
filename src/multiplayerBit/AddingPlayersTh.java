package multiplayerBit;

import main.Player;
import main.Server;
import multiplayerBit.IncomingMessages;

import java.io.BufferedWriter;
import java.io.OutputStreamWriter;
import java.net.ServerSocket;
import java.net.Socket;

public class AddingPlayersTh implements Runnable{
    private Thread t;


    @Override
    public void run() {
        try{
            ServerSocket listener = new ServerSocket(4000);
            try{

                System.out.println("Server listening for players.");
                while (true){
                    Socket socket = listener.accept();
                    System.out.println("Player found.");
                    Player player = new Player();
                    IncomingMessages incMess = new IncomingMessages(socket, player);
                    player.messageThread = incMess;
                    incMess.start();



                    Server.world.allPlayers.add(player);
                }
            }finally{
                listener.close();
            }
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    public void start () {
        if (t == null) {
            t = new Thread (this, "AddingPlayers");
            t.start ();
        }
    }

    public static void startLooking(){
        new AddingPlayersTh().start();
    }


}
