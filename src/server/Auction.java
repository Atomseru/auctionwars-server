package server;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;

public class Auction {
    public ArrayList<SItem> items = new ArrayList<>();


    static String priceLookingFor = "a-size-base a-color-price s-price a-text-bold\">";
    static String imageLookingFor = "<img src=\"";

    public void regenerateRandomItems(){

    }
    public ArrayList<SItem> getWebListings(String searchTerm){
        try{
            URL url = new URL("      https://www.amazon.co.uk/s/ref=nb_sb_noss?url=search-alias%3Daps&field-keywords=" + searchTerm + "     ");

            URLConnection con = url.openConnection();
            InputStream is =con.getInputStream();

            BufferedReader br = new BufferedReader(new InputStreamReader(is));

            String line = null;

            int lnNum = 1;
            ArrayList<SItem> allItems = new ArrayList<>();
            while ((line = br.readLine()) != null) {
                //System.out.println(line);

                Integer priceIndex = line.indexOf(priceLookingFor);
                Integer imageIndex = line.indexOf(imageLookingFor);


                if(priceIndex != -1 && imageIndex != -1){
                    SItem item = new SItem();
                    item.cost = doPrice(line, priceIndex);
                    item.imageURL = doImage(line, imageIndex);
                    allItems.add(item);
                }

                lnNum ++;
            }
            return allItems;
        }catch(Exception e){
            e.printStackTrace();
        }
        return null;
    }

    private static String doImage(String line, Integer index){
        Integer hypStart = index + imageLookingFor.length();
        String rough = line.substring(hypStart , hypStart + 200);

        String fine = rough.substring(0, rough.indexOf("\""));
        System.out.println("Fine is " + fine);
        try{
            return fine;
        }catch(Exception e){
            e.printStackTrace();
        }
        return null;
    }
    private static Double doPrice(String line, Integer index){
        Integer from = index  + priceLookingFor.length();
        String priceStr = line.substring(from, from + 20);

        Integer poundSign = priceStr.indexOf("£");
        Integer arrow = priceStr.indexOf("<");
        String price = priceStr.substring(poundSign + 1, arrow);

        if(price.contains("-")){
            // price = "7.99 - £4.99"
            String firstNumber = price.substring(0, price.indexOf(" "));
        }

        System.out.println("Price is £" + price);
        return Double.parseDouble(price);
    }
}
